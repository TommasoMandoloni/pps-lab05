package u05lab.code

trait Kind

object Kind {
  RETIRED
  FAILED
  SUCCEDED
}

case class RETIRED() extends Kind
case class FAILED() extends Kind
case class SUCCEDED() extends Kind

sealed trait Result {

  def getKind: Kind

  def getEvaluation: Option[Int]

  def cumLaude: Boolean

}

sealed trait ExamsManager {

  def createNewCall(call: String): Unit

  def addStudentResult(call: String, student: String, res: Result): Unit

  def getAllStudentsFromCall(call: String): Set[String]

  def getEvaluationMapFromCall(call: String): Map[String, Int]

  def getResultsMapFromStudent(student: String): Map[String, String]

  def getBestResultFromStudent(student: String): Option[Int]

}

case class ExamsManagerImplementation() extends ExamsManager {

  private var exams: Map[String, Map[String, Result]] = Map.empty

  val check: Boolean => Unit = cond => if (!cond) throw new IllegalArgumentException

  override def createNewCall(call: String): Unit = {
    check(!exams.contains(call))
    exams += (call -> Map.empty)
  }

  override def addStudentResult(call: String, student: String, res: Result): Unit = {
    check(exams.contains(call))
    check(!exams(call).contains(student))
    var map: Map[String, Result] = exams(call)
    map += (student -> res)
    exams += (call -> map)
  }

  override def getAllStudentsFromCall(call: String): Set[String] = {
    check(exams.contains(call))
    exams(call).keySet
  }

  override def getEvaluationMapFromCall(call: String): Map[String, Int] = {
    check(exams.contains(call))
    exams(call).collect({case (s, r) if r.getKind.equals(SUCCEDED()) => s -> r.getEvaluation.get})
  }

  override def getResultsMapFromStudent(student: String): Map[String, String] = {
    exams.collect({case (c, m) if m.contains(student) => c -> exams(c)(student).toString})
  }

  override def getBestResultFromStudent(student: String): Option[Int] = {
    exams.collect({case (c, m) if m.contains(student) => c -> m(student).getEvaluation.getOrElse(0)}).max._2 match {
      case 0 => Option.empty
      case i => Option(i)
    }
  }

}

sealed trait ExamResultFactory {

  def failed: Result

  def retired: Result

  def succededCumLaude: Result

  def succeded(evaluation: Int): Result

}

object ExamResult extends ExamResultFactory {

  private case class AbstractExamResult(kind: Kind, evaluation: Option[Int] = Option.empty, laude: Boolean = false) extends Result {

    evaluation match {
      case Some(value) if value < 18 || value > 30 => throw new IllegalArgumentException
      case _ => evaluation
    }

    override def getKind: Kind = kind

    override def getEvaluation: Option[Int] = evaluation

    override def cumLaude: Boolean = laude

    override def toString: String = kind match {
      case SUCCEDED() if cumLaude && evaluation.get == 30 => "SUCCEDED(30L)"
      case FAILED() => "FAILED"
      case RETIRED() => "RETIRED"
      case _ => "SUCCEDED(" + evaluation.get +")"
    }

  }

  override def failed: Result = AbstractExamResult(FAILED())

  override def retired: Result = AbstractExamResult(RETIRED())

  override def succededCumLaude: Result = AbstractExamResult(SUCCEDED(), Option(30), true)

  override def succeded(evaluation: Int): Result = AbstractExamResult(SUCCEDED(), Option(evaluation))
}

