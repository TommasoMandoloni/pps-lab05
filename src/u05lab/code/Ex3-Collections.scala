package u05lab.code

import java.util.concurrent.TimeUnit
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.FiniteDuration

object PerformanceUtils {
  case class MeasurementResults[T](result: T, duration: FiniteDuration) extends Ordered[MeasurementResults[_]] {
    override def compare(that: MeasurementResults[_]): Int = duration.toNanos.compareTo(that.duration.toNanos)
  }

  def measure[T](msg: String)(expr: => T): MeasurementResults[T] = {
    val startTime = System.nanoTime()
    val res = expr
    val duration = FiniteDuration(System.nanoTime()-startTime, TimeUnit.NANOSECONDS)
    if(!msg.isEmpty) println(msg + " -- " + duration.toNanos + " nanos; " + duration.toMillis + "ms")
    MeasurementResults(res, duration)
  }

  def measure[T](expr: => T): MeasurementResults[T] = measure("")(expr)
}


object CollectionsTest extends App {

  /* Linear sequences: List, ListBuffer */

  println("*** LINEAR SEQUENCES ***")

  var l: scala.List[Int] = scala.List(10, 20, 30)
  val lb = ListBuffer[Int]()

  l = l.appended(40)

  lb += 10
  lb += 20
  lb += 30
  lb += 40

  println(l, lb)

  println(l(1), lb(1))

  l = l.drop(1)
  lb -= 10

  println(l, lb)

  /* Indexed sequences: Vector, Array, ArrayBuffer */

  println("*** INDEXED SEQUENCES ***")

  import collection._
  var v: Vector[Int] = Vector(10,20,30)
  var a: Array[Int] = Array(10,20,30)
  val b: mutable.ArrayBuffer[Int] = mutable.ArrayBuffer[Int]()

  v = v.appended(40)
  a = a.appended(40)
  b += (10, 20, 30, 40)

  println(v, a.mkString("Array(", ", ", ")"), b)

  println(v(1), a(1), b(1))

  v = v.drop(1)
  a = a.drop(1)
  b -= 10

  println(v, a.mkString("Array(", ", ", ")"), b)

  /* Sets */

  println("*** SETS ***")

  var s: Set[Int] = Set(10, 20, 30)
  val ms: mutable.Set[Int] = mutable.Set(10,20,30)

  s += 40
  ms += 40

  println(s, ms)

  println(s.head, ms.head)

  s -= 10
  ms -= 10

  println(s, ms)

  /* Maps */

  println("*** MAPS ***")

  var m: Map[Int, String] = Map(10 -> "a", 20 -> "b", 30 -> "c")
  var mm: mutable.Map[Int,String] = mutable.Map[Int,String]()

  mm += (10 -> "a")
  mm += (20 -> "b")
  mm += (30 -> "c")

  println(m ,mm)

  println(m(10), mm(10))

  mm = mm.drop(1)
  m = m.drop(1)

  println(m ,mm)

  /* Comparison */
  import PerformanceUtils._
  val lst = (1 to 1000000).toList
  val vec = (1 to 1000000).toVector
  assert( measure("lst last"){ lst.last } > measure("vec last"){ vec.last } )
}