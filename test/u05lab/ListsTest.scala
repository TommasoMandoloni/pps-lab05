package u05lab

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u05lab.code.List
import u05lab.code.List.sequence

class ListsTest {

  @Test
  def testZipRight(): Unit = {
    val l = List("a","b","c")
    assertEquals(List.nil, List.nil.zipRight)
    assertEquals(List(("a",0), ("b",1), ("c",2)), l.zipRight)
  }

  @Test
  def testPartition(): Unit = {
    val l = List(15, 17, 18, 10, 3, 20)
    assertEquals((15 :: 10 :: 20 :: List.nil, 17 :: 18 :: 3 :: List.nil), l.partition(_ % 5 == 0))
    assertEquals((List.nil, l), l.partition(_ % 7 == 0)) // no one satisfy predicate
    val list = List(2, 4, 6, 0, 8)
    assertEquals((list, List.nil), list.partition(_ % 2 == 0)) // every element satisfy predicate
  }

  @Test
  def testSpan(): Unit = {
    val l = List(15, 17, 18, 10, 3, 20)
    assertEquals((List(15, 17, 18), List(10, 3 ,20)), l.span(_ > 11))
    assertEquals((List.nil, l), l.span(_ == 5)) // no one satisfy predicate
    val list = List(2, 4, 6, 0, 8)
    assertEquals((list, List.nil), list.span(_ % 2 == 0)) // every element satisfy predicate
  }

  @Test
  def testReduce(): Unit = {
    val l = List(0, 2, 4, 6, 8, 10)
    assertEquals(30, l.reduce(_+_))
    assertThrows(classOf[UnsupportedOperationException], () => List.nil[Int].reduce(_+_))
  }

  @Test
  def testTakeRight(): Unit = {
    val l = List(10, 20, 30, 40, 50, 60)
    assertEquals(List.nil, l.takeRight(0))
    assertEquals(List(40, 50, 60), l.takeRight(3))
  }

  @Test
  def testCollect(): Unit = {
    val l = List(10, 20, 30, 40, 50, 60)
    assertEquals(List.nil, l.collect({case x if x == 100 => x}))
    assertEquals(List(11, 21), l.collect({case x if x < 30 => x+1}))
  }

  @Test
  def testSequence(): Unit = {
    var list: List[Option[Int]] = List(Some(10), Some(20), Some(30))
    assertEquals(Some(List(10,20,30)), sequence(list))
    list = List(Some(10), None, Some(30))
    assertEquals(None, sequence(list))
  }

}