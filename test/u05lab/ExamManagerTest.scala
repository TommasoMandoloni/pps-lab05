package u05lab

import org.junit.jupiter.api.Assertions.{assertEquals, assertFalse, assertThrows, assertTrue}
import org.junit.jupiter.api.Test
import u05lab.code.{ExamResult, ExamResultFactory, ExamsManager, ExamsManagerImplementation, FAILED, RETIRED, SUCCEDED}

class ExamManagerTest {

  val erf: ExamResultFactory = ExamResult
  val em: ExamsManager = ExamsManagerImplementation()

  @Test
  def testExamResultBasicBehaviour(): Unit = {
    assertEquals(erf.failed.getKind, FAILED())
    assertFalse(erf.failed.getEvaluation.isDefined)
    assertFalse(erf.failed.cumLaude)

    assertEquals(erf.retired.getKind, RETIRED())
    assertFalse(erf.retired.getEvaluation.isDefined)
    assertFalse(erf.retired.cumLaude)

    assertEquals(erf.succededCumLaude.getKind, SUCCEDED())
    assertEquals(erf.succededCumLaude.getEvaluation, Option(30))
    assertTrue(erf.succededCumLaude.cumLaude)

    assertEquals(erf.succeded(28).getKind, SUCCEDED())
    assertEquals(erf.succeded(28).getEvaluation, Option(28))
    assertFalse(erf.succeded(28).cumLaude)
  }

  @Test
  def testException(): Unit = {
    assertThrows(classOf[java.lang.IllegalArgumentException], () => erf.succeded(31))
    assertThrows(classOf[java.lang.IllegalArgumentException], () => erf.succeded(17))
    this.prepareExams()
    assertThrows(classOf[java.lang.IllegalArgumentException], () => em.createNewCall("Marzo"))
    assertThrows(classOf[java.lang.IllegalArgumentException], () => em.addStudentResult("Gennaio", "Verdi", erf.failed))
  }

  private def prepareExams(): Unit = {
    em.createNewCall("Gennaio")
    em.createNewCall("Febbraio")
    em.createNewCall("Marzo")

    em.addStudentResult("Gennaio", "Rossi", erf.failed)
    em.addStudentResult("Gennaio", "Bianchi", erf.retired)
    em.addStudentResult("Gennaio", "Verdi", erf.succeded(28))
    em.addStudentResult("Gennaio", "Neri", erf.succededCumLaude)

    em.addStudentResult("Febbraio", "Rossi", erf.failed)
    em.addStudentResult("Febbraio", "Bianchi", erf.succeded(20))
    em.addStudentResult("Febbraio", "Verdi", erf.succeded(30))

    em.addStudentResult("Marzo", "Rossi", erf.succeded(25))
    em.addStudentResult("Marzo", "Bianchi", erf.succeded(25))
    em.addStudentResult("Marzo", "Gialli", erf.failed)
  }

  @Test
  def testExamManagement(): Unit = {
    this.prepareExams()
    assertEquals(em.getAllStudentsFromCall("Gennaio"), Set("Rossi", "Bianchi", "Verdi", "Neri"))
    assertEquals(em.getAllStudentsFromCall("Marzo"), Set("Rossi", "Bianchi", "Gialli"))

    assertEquals(em.getEvaluationMapFromCall("Gennaio").size, 2)
    assertEquals(em.getEvaluationMapFromCall("Gennaio")("Verdi"), 28)
    assertEquals(em.getEvaluationMapFromCall("Gennaio")("Neri"), 30)

    assertEquals(em.getEvaluationMapFromCall("Febbraio").size, 2)
    assertEquals(em.getEvaluationMapFromCall("Febbraio")("Bianchi"), 20)
    assertEquals(em.getEvaluationMapFromCall("Febbraio")("Verdi"), 30)

    assertEquals(em.getResultsMapFromStudent("Rossi").size, 3)
    assertEquals(em.getResultsMapFromStudent("Rossi")("Gennaio"), "FAILED")
    assertEquals(em.getResultsMapFromStudent("Rossi")("Febbraio"), "FAILED")
    assertEquals(em.getResultsMapFromStudent("Rossi")("Marzo"), "SUCCEDED(25)")

    assertEquals(em.getResultsMapFromStudent("Bianchi").size, 3)
    assertEquals(em.getResultsMapFromStudent("Bianchi")("Gennaio"), "RETIRED")
    assertEquals(em.getResultsMapFromStudent("Bianchi")("Febbraio"), "SUCCEDED(20)")
    assertEquals(em.getResultsMapFromStudent("Bianchi")("Marzo"), "SUCCEDED(25)")

    assertEquals(em.getResultsMapFromStudent("Neri").size, 1)
    assertEquals(em.getResultsMapFromStudent("Neri")("Gennaio"), "SUCCEDED(30L)")

    assertEquals(em.getBestResultFromStudent("Rossi"), Option(25))
    assertEquals(em.getBestResultFromStudent("Bianchi"), Option(25))
    assertEquals(em.getBestResultFromStudent("Neri"), Option(30))
    assertEquals(em.getBestResultFromStudent("Gialli"), Option.empty)
  }

}
